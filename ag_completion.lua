-- AG: I took this function from completion.lua and remade it for my usage

local gfs = require("gears.filesystem")

-- Grab environment we need
local io = io
local os = os
local table = table
local math = math
local print = print
local pairs = pairs
local string = string

local gears_debug = require("gears.debug")
local gstring = require("gears.string")

local completion = {}

-- mapping of command/completion function
local bashcomp_funcs = {}
local bashcomp_src = "/etc/bash_completion"
-- ag: my location is different:
bashcomp_src = "/usr/share/bash-completion/bash_completion"

local function bash_escape(str)
    str = str:gsub(" ", "\\ ")
    str = str:gsub("%[", "\\[")
    str = str:gsub("%]", "\\]")
    str = str:gsub("%(", "\\(")
    str = str:gsub("%)", "\\)")
    return str
end

local naughty = require("naughty")

local function log(text)
   naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Log",
                     text = text })
end

function ag_completion(command, cur_pos, ncomp, shell)
   local wstart = 1
   local wend = 1
   local words = {}
   local cword_index = 0
   local cword_start = 0
   local cword_end = 0
   local i = 1
   local comptype = "file"

   -- do nothing if we are on a letter, i.e. not at len + 1 or on a space
   if cur_pos ~= #command + 1 and command:sub(cur_pos, cur_pos) ~= " " then
      return command, cur_pos
   elseif #command == 0 then
      return command, cur_pos
   end

   while wend <= #command do
      wend = command:find(" ", wstart)
      if not wend then wend = #command + 1 end
      table.insert(words, command:sub(wstart, wend - 1))
      if cur_pos >= wstart and cur_pos <= wend + 1 then
	 cword_start = wstart
	 cword_end = wend
	 cword_index = i
      end
      wstart = wend + 1
      i = i + 1
   end

   if cword_index == 1 then
      comptype = "command"
   end

   local shell_cmd
   if not shell then
      if not completion.default_shell then
	 local env_shell = os.getenv('SHELL')
	 if not env_shell then
	    gears_debug.print_warning('SHELL not set in environment, falling back to bash.')
	    completion.default_shell = 'bash'
	 elseif env_shell:match('zsh$') then
	    completion.default_shell = 'zsh'
	 else
	    completion.default_shell = 'bash'
	 end
      end
      shell = completion.default_shell
   end
   if shell == 'zsh' then
      log("Zsh is not implemented!")
      -- AG: Not implemented
   else
      if bashcomp_funcs[words[1]] then
	 -- AG: I haven't seen this if in execution yet so I'm leaving
	 -- it unchanged
	 
	 -- fairly complex command with inline bash script to get the possible completions
	 shell_cmd = "/usr/bin/env bash -c 'source " .. bashcomp_src
	    .. " && source ~/.bashrc && " ..
	    "__print_completions() { for ((i=0;i<${#COMPREPLY[*]};i++)); do echo ${COMPREPLY[i]}; done }; " ..
	    "COMP_WORDS=(" ..  command .."); COMP_LINE=\"" .. command .. "\"; " ..
	    "COMP_COUNT=" .. cur_pos ..  "; COMP_CWORD=" .. cword_index-1 .. "; " ..
	    bashcomp_funcs[words[1]] .. "; __print_completions'"
      else
	 shell_cmd = "/usr/bin/env bash -c '~/.local/bin/ag-complete.sh"
	    .. " " .. string.format('%q', words[cword_index]) .. "'"
      end
   end
   local c, err = io.popen(shell_cmd .. " | tr ' ' '\n' | sort -u")
   local output = {}
   if c then
      while true do
	 local line = c:read("*line")
	 if not line then break end
	 if gstring.startswith(line, "./") and gfs.is_dir(line) then
	    line = line .. "/"
	 end
	 table.insert(output, bash_escape(line))
      end

      c:close()
   else
      print(err)
   end

   -- no completion, return
   if #output == 0 then
      return command, cur_pos
   end

   -- cycle
   while ncomp > #output do
      ncomp = ncomp - #output
   end

   local str = command:sub(1, cword_start - 1) .. output[ncomp] .. command:sub(cword_end)
   cur_pos = cword_start + #output[ncomp]

   return str, cur_pos, output
end
