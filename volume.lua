local wibox = require("wibox")
local awful = require("awful")

Volume = {}

function Volume:new()
   new = {}
   setmetatable(new, self)
   self.__index = self

   new.widget = wibox.widget.textbox()
   new.widget:set_align("right")

   new:update()
   
   return new
end

function Volume:update()
   awful.spawn.easy_async_with_shell(
      "amixer sget Master",
      function(status)
	 -- local volume = tonumber(string.match(status, "(%d?%d?%d)%%")) / 100
	 local volume = string.match(status, "(%d?%d?%d)%%")
	 volume = string.format("% 3d", volume)
 
	 status = string.match(status, "%[(o[^%]]*)%]")
	 
	 if string.find(status, "on", 1, true) then
	    -- For the volume numbers
	    volume = volume .. "%"
	 else
	    -- For the mute button
	    volume = volume .. "#"
	 end
	 
	 self.widget:set_markup(volume)
      end
   )
end

function Volume:increase()
   awful.spawn.easy_async_with_shell(
      "amixer set Master 3%+",
      function () self:update() end
   )
end

function Volume:decrease()
   awful.spawn.easy_async_with_shell(
      "amixer set Master 3%-",
      function () self:update() end
   )
end

function Volume:mute()
   awful.spawn.easy_async_with_shell(
      "amixer set Master toggle",
      function () self:update() end
   )
end

return Volume:new()
