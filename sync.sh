#!/usr/bin/env bash

cpath="/home/ag/.config/awesome"
theme_path="/usr/share/awesome/themes/gtheme"
rpath="/home/ag/.config/config-git-repositories/awesomewm-config"

cp $cpath/ag_completion.lua $cpath/screenshot.lua $cpath/volume.lua $cpath/battery.lua $cpath/rc.lua $rpath
cp -r $theme_path $rpath/gtheme
