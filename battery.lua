local wibox = require("wibox")
local awful = require("awful")

Battery = {}

function Battery:new()
   new = {}
   setmetatable(new, self)
   self.__index = self

   new.widget = wibox.widget.textbox()
   new.widget:set_align("right")

   new:update()
   
   return new
end

function Battery:update(widget)
   awful.spawn.easy_async_with_shell(
      "acpi",
      function(status)
	 local display = string.match(status, "(%d?%d?%d)%%")
	 display = string.format("% 3d", display)
	 
	 if string.find(status, "Discharging", 1, true) then
	    display = display .. "%"
	 else
	    display = display .. "~"
	 end
	 
	 self.widget:set_markup(display)
      end
   )
end

return Battery:new()
